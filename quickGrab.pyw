from PIL import ImageGrab
import os
import time

"""

All coordinates assume a screen resolution of 1920x1024 and opera
 maximized with no tool bars enabled
Downkey has not been hit at all
x_pad = 467 (we rounded to the nearest whole number for simplicity
y_pad = 224 (again we rounded to the nearest whole number

Like all screen coords.

Play area = x_pad+1, y_pad+1, 1105.5, 702.5
"""

# Globals
# ----------

x_pad = 467
y_pad = 224


def screenGrab():
    box = (x_pad+1, y_pad+1, x_pad+639, y_pad+479)
    im = ImageGrab.grab()
    im.save(os.getcwd() + '\\full_snap__' + str(int(time.time())) +
            '.png', 'PNG')


def main():
    screenGrab()

if __name__ == '__main__':
    main()
