import os
import time

"""

Explain the scope of your project variables and give a summary
"""
def add():
    x = float(input('Enter a number: '))
    y = float(input('Enter a second number to Add by: '))

    print(x + y)

def subtract():
    a = float(input('Enter a number: '))
    b = float(input('Enter a second number to subtract by: '))

    print(a - b)

def getOperation():
    op = input('Enter 1 for addition enter 2 for subtraction: ')
    if op == '1':
        add()
    else:
        subtract()

def main():
    getOperation()

if __name__ == '__main__':
    main()

